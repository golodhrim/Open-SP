# Welcome to the Open Synthetic Phonics curriculum! #

****

If you are a teacher, you will find completed materials ready to use in the Final folder.

If you want to help develop the curriculum, you will need the following materials, all of which are public domain, except as noted:

* a copy of the [LibreOffice][LO] Suite
* the font [Aurulent Sans][Aurulent]
* the font [Goudy Bookletter 1911][Goudy]
* the font [MPH 2B Damase][MPH]
* the set of handwriting fonts by Educational Fontware, Inc., which are commercial and cost $50 for an individual. You get them from [Educational Fontware][handwriting].
* materials already created, which are in the Development folder

Good luck, and have fun contributing to this curriculum!

If you encounter problems with your downloads, please have a look at our [Wiki][wiki]. If that hasn't helped, you can open an issue or contact us in IRC at [irc://irc.freenode.net/frogandowl][IRC]. We will do our best to help you obtain the files.

[LO]: http://libreoffice.org/ "LibreOffice Homepage"
[Aurulent]: http://www.dafont.com/aurulent-sans.font "Aurulent Sans Font"
[Goudy]: http://www.dafont.com/goudy-bookletter.font "Goudy Bookletter 1911 Font"
[MPH]: http://www.dafont.com/mph-2b-damase.font "MPH 2B Damase Font"
[handwriting]: http://www.educationalfontware.com/ "Educational Fontware"
[wiki]: http://github.com/FrogandOwl/Open-SP/wiki "Open-SP Wiki"
[IRC]: irc://irc.freenode.net/frogandowl "Frog and Owl at IRC FreeNode"
